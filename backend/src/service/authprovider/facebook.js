/* eslint linebreak-style: ["error", "windows"] */

import axios from 'axios';

const FIELDS = 'name,picture,email';

const BASE_URL = `https://graph.facebook.com/me?fields=${FIELDS}`;

export const authSync = async (token) => {
  try {
    const res = await axios.get(`${BASE_URL}&access_token=${token}`);

    if (res.status === 200) {
      return res.data;
    }

    throw new Error('No success with Facebook');
  } catch (error) {
    throw error;
  }
};

export const facebook = {
  authSync,
};
