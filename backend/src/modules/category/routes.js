import express from 'express';
import {
  create,
  get,
  getAll,
  update,
  deleteCategorys,
  deleteCategoryProducts,
  AddCategoryProduct,
} from './controller';

const routes = express.Router();

routes.post('/', create);
routes.get('/:categoryId', get);
routes.get('/', getAll);
routes.put('/', update);
routes.delete('/:categoryId', deleteCategorys);

routes.put('/add/:categoryId/:productId', AddCategoryProduct);
routes.put('/delete/:categoryId/:productId', deleteCategoryProducts);

export default routes;
