import mongoose from 'mongoose';
import Category from './models';
import Product from '../products/model';

const { ObjectId } = mongoose.Types;

/**
 * This function create a category
 * @async
 * @function createCategory
 * @param {String} category
 * @returns Promise
 * @example
 * createCategory('grocery')
 * .then((e) => console.log(e))
 * .catch((e) => console.error(e));
 */
export const createCategory = async (category) => {
  try {
    const _category = await Category.findOne({ category });

    if (!_category) {
      const categories = await Category.create({
        name: category,
      });

      return categories;
    }
    return new Error('Category already existed');
  } catch (error) {
    throw error;
  }
};

// createCategory('paper products')
//   .then((e) => console.log(e))
//   .catch((e) => console.error(e));

/**
 * This function get a category from the category
 * @async
 * @function getCategory
 * @param {String} category
 * @returns Promise
 * @example
 * getCategory('grocery')
 * .then((e) => console.log(e))
 * .catch((e) => console.error(e))
 */
export const getCategory = async (category) => {
  try {
    const _category = await Category.findOne({ name: category });

    if (!_category) {
      throw new Error('Category does not exist');
    }

    return _category;
  } catch (error) {
    throw error;
  }
};

// getCategory('paper products')
//   .then((e) => console.log(e))
//   .catch((e) => console.error(e));

/**
 * This function return all the Category
 * @async
 * @function getAllCategory
 * @returns Promise
 * @example
 * getAllCategory()
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e))
 */
export const getAllCategory = async () => {
  try {
    const _category = await Category.find()
      .populate('products')
      .populate('products', 'name', Product)
      .exec();
      // .exec((err, res) => {
      //   console.log(res);
      // });

    return _category;
  } catch (error) {
    throw error;
  }
};

// getAllCategory()
//   .then((e) => console.log(...e))
//   .catch((e) => console.log(e));

/**
 * @async
 * @function updateCategory
 * @param {String} category
 * @param {String} updatedName
 * @returns Promise
 * @example
 * updateCategory('paper products', 'Paper')
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const updateCategory = async (category, updatedName) => {
  try {
    const _category = await Category.findOne({ name: category });

    if (!_category) {
      throw new Error('Cannot find the category');
    }

    const categoryUpdated = await Category
      .findOneAndUpdate({ name: category }, { name: updatedName });

    return categoryUpdated;
  } catch (error) {
    throw error;
  }
};

// updateCategory('paper products', 'Paper')
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * @async
 * @function deleteCategory
 * @param {String} category
 * @returns Promise
 * @example
 * deleteCategory('paper products')
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const deleteCategory = async (category) => {
  try {
    const _category = await Category.findOne({ name: category });

    if (!_category) {
      throw new Error('category does not exist');
    }
    const deletedCategory = await Category.deleteOne({ name: category });

    return deletedCategory;
  } catch (error) {
    throw error;
  }
};

// deleteCategory('paper products')
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * @async
 * @function AddproductToCategory
 * @param {String} category
 * @param {String} productId
 * @returns Promise
 * @example
 * AddproductToCategory('Paper', '5b97a6653901b229d850ae38')
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const AddproductToCategory = async (category, productId) => {
  try {
    const _category = await Category.findOne({ name: category });

    if (!_category) {
      throw new Error('Category does not exist');
    }

    const products = _category
      .products
      .find((element) => element.toString() === productId);

    if (products === undefined) {
      const Allproducts = [..._category.products, productId];
      const _products = await Category
        .updateOne({ name: category }, { products: Allproducts });

      return 'Added to the Category';
    }
    return 'Already on the category';
  } catch (error) {
    throw error;
  }
};
// AddproductToCategory('paper products', '5b9b9abcf10b6b13105f9495')
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function add the array of the productsId to the category
 * @async
 * @function AddArrayofProductsToCategory
 * @author Palash Gupta
 * @param {String} id
 * @param {[String]} arrayOfProductId
 * @returns Promise
 * @example
 * AddArrayofProductsToCategory('5b9a0629c9034019b4431568',
 * ['5b9e002a0b9e7a32849f160c', '5b9dffcc9ca1162d44d11663'])
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
const AddArrayofProductsToCategory = async (id, arrayOfProductId) => {
  try {
    const category = await Category.findOne({ _id: id });

    if (!category) {
      throw new Error('Category Does not exist');
    }


    const covertToObjectId = (array) => {
      const arrayOfId = [];
      for (let i = 0; i < array.length; i++) {
        arrayOfId.push(ObjectId(arrayOfId[i]));
      }
      return arrayOfId;
    };

    const products = [...category.products,
      ...(covertToObjectId)(arrayOfProductId)];


    const _updated = await Category.updateOne({ _id: id }, { products });

    return _updated;
  } catch (error) {
    throw error;
  }
};

// AddArrayofProductsToCategory('5b9a0629c9034019b4431568',
//   ['5b9e002a0b9e7a32849f160c', '5b9dffcc9ca1162d44d11663'])
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * @async
 * @function deleteCategoryProduct
 * @param {String} category
 * @param {String} productId
 * @returns Promise
 * @example
 * deleteCategoryProduct('Paper', '5b97a6653901b229d850ae38')
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const deleteCategoryProduct = async (category, productId) => {
  try {
    const _category = await Category.findOne({ name: category });

    if (!_category) {
      throw new Error('category does not exist');
    }

    const products = _category.products
      .find((element) => element.toString() === productId);

    if (products === undefined) {
      return 'product not found';
    }

    const index = _category.products.findIndex((e) => e === productId);

    const product = [..._category.products.splice(0, index),
      ..._category.products.splice(index, _category.products.length)];

    const updatedProducts = await Category
      .updateOne({ name: category }, { products: product });

    return 'deleted';
  } catch (error) {
    throw error;
  }
};

// deleteCategoryProduct('Paper', '5b97a6653901b229d850ae38')
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));


/**
 * @async
 * @function deleteAllCategoryProduct
 * @param {String} id
 * @returns Promise
 * @example
 */
const deleteAllCategoryProduct = async (id) => {
  try {
    const category = await Category.findById(id);

    if (!category) {
      throw new Error('No category exist');
    }

    const _updated = Category.updateOne({ _id: id }, { products: [] });

    return _updated;
  } catch (error) {
    throw error;
  }
};

// deleteAllCategoryProduct()
//   .then(e=>console.log(e))
//   .catch(e=>console.log(e));

const populate = async (id) => {
  try {
    const product = await Category
      .find({ _id: id })
      .populate('products', 'name price', Product)
      .exec();


    // const productsasd = product.products;
    // await productsasd.populate('products').exec();

    return product;
    // console.log(product);
  } catch (error) {
    throw error;
  }
};

// populate('5b9a0629c9034019b4431568')
//   .then((e) => console.log(...e))
//   .catch((e) => console.log(e));
