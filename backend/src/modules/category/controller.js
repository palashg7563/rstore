import * as Yup from 'yup';
import {
  createCategory,
  AddproductToCategory,
  getCategory,
  getAllCategory,
  updateCategory,
  deleteCategory,
  deleteCategoryProduct,
} from './category';


export const create = async (req, res) => {
  try {
    const { name } = req.body;

    const bodyScehma = Yup.object().shape({
      name: Yup.string().strict(true).required(),
    });

    await bodyScehma.validateSync({ name });

    const categories = await createCategory(name);

    res.json({ created: true, id: categories._id });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const get = async (req, res) => {
  try {
    const { categoryId: category } = req.params;

    const bodyScehma = Yup.object().shape({
      category: Yup.string().required().strict(true),
    });

    await bodyScehma.validateSync({ category });

    const categorys = await getCategory(category);

    res.json({ category: categorys });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const getAll = async (req, res) => {
  try {
    const response = await getAllCategory();

    res.json({ category: response });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const update = async (req, res) => {
  try {
    const { category, updatedName } = req.body;

    const bodyScehma = Yup.object().shape({
      category: Yup.string().required().strict(true),
      updatedName: Yup.string().required().strict(true),
    });

    await bodyScehma.validateSync({ category, updatedName });
    const response = await updateCategory(category, updatedName);

    res.json({ res: response });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};


export const deleteCategorys = async (req, res) => {
  try {
    const { categoryId: category } = req.params;

    const bodyScehma = Yup.object().shape({
      category: Yup.string().strict(true).required(),
    });

    await bodyScehma.validateSync({ category });
    const categorys = await deleteCategory(category);

    res.json({ response: categorys });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};

export const AddCategoryProduct = async (req, res) => {
  try {
    const { categoryId: name, productId: products } = req.params;

    const bodyScehma = Yup.object().shape({
      products: Yup.array(Yup.string()).required(),
    });

    await bodyScehma.validateSync(products);

    const product = await AddproductToCategory(name, products);

    res.json({ message: product });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};


export const deleteCategoryProducts = async (req, res) => {
  try {
    const { categoryId: category, productId } = req.params;

    const bodyScehma = Yup.object().shape({
      category: Yup.string().required().strict(true),
      productId: Yup.string().required().strict(true),
    });

    await bodyScehma.validateSync({ category, productId });
    const response = await deleteCategoryProduct(category, productId);

    res.json({ response });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};
