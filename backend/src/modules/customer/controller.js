/* eslint linebreak-style: ["error", "windows"] */

import * as Yup from 'yup';

import { PROVIDER_ENUM } from './model';
import { AuthProvider } from '../../service/authprovider/index';
import { getOrCreateCustomer, me } from './customer';
import { AuthServices } from '../../service/auth';

export const create = async (req, res) => {
  try {
    const { token, provider } = req.body;

    const bodySchema = Yup.object().shape({
      token: Yup.string().required(),
      provider: Yup.string().oneOf(PROVIDER_ENUM).required(),
    }).required();

    await bodySchema.validate({ token, provider });

    let data;

    if (provider === 'FACEBOOK') {
      data = await AuthProvider.facebook.authSync(token);
    } else if (provider === 'GOOGLE') {
      data = await AuthProvider.google.authSync(token);
    } else {
      res.sendStatus(400);
    }

    const customer = await getOrCreateCustomer(data, provider);

    const jwtToken = AuthServices.createToken(customer);

    res.status(201).json({ token: jwtToken });
  } catch (error) {
    res.status(404).json({ error: error.message });
  }
};

export const getUserInfo = async (req, res) => {
  try {
    if (req.user) {
      const userInfo = await me(req.user._id);

      res.status(200).json(userInfo);
    } else {
      res.status(400).json({ message: 'No User' });
    }
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
