/* eslint linebreak-style: ["error", "windows"] */

import { Router } from 'express';
import { create, getUserInfo } from './controller';
import { customerAuth } from './customer';

const routes = Router();

routes.post('/', create);

routes.get('/me', customerAuth, getUserInfo);

export default routes;
