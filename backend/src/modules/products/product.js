/**
 * This file contain all the database function of the products
 */

import Products from './model';

/**
 * this function will create the product
 * @async
 * @function createProduct
 * @param {String} name
 * @param {String} description
 * @param {Number} price
 * @param {String} productAvatar
 * @returns Promise
 * @example
 * createProduct('7 No', 'size of the no', 8, 'https://google.com')
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const createProduct = async (name, description, price, productAvatar) => {
  try {
    const _product = await Products.findOne({ name });

    if (!_product) {
      const products = await Products.create({
        name,
        description,
        price,
        productAvatar,
      });

      return { id: products._id };
    }
    throw new Error('Product all ready exist');
  } catch (error) {
    throw error;
  }
};

// createProduct('10 No', 'size of the no', 8, 'https://google.com')
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * this function get a product
 * @async
 * @function getProduct
 * @param {String} product
 * @returns Promise
 * @example
 * getProduct('7 No')
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const getProduct = async (name) => {
  try {
    const _product = await Products.findById(name);

    if (!_product) {
      throw new Error('product does not exist');
    }

    return _product;
  } catch (error) {
    throw error;
  }
};

// getProduct('7 No')
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * this  function getAll the product
 * @async
 * @function getAllProduct
 * @returns Promise
 * @example
 * getAllProduct()
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const getAllProduct = async () => {
  try {
    const _product = await Products.find({}).sort({ createdAtDate: 1 });

    return _product;
  } catch (error) {
    throw error;
  }
};

// getAllProduct()
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * this function update the given value of the database
 * @async
 * @function updateProduct
 * @param {String} product
 * @param {[String]} updatedProduct
 * @returns Promise
 * @example
 * updateProduct('7 no', { name: 'asdasd', price: 123, description: 'asdasdasd' })
 */
export const updateProduct = async (name, updatedProduct) => {
  try {
    const _updateProduct = await Products
      .findOneAndUpdate({ name }, { ...updatedProduct });

    return _updateProduct;
  } catch (error) {
    throw error;
  }
};

// updateProduct('7 no', { name: 'asdasd', price: 123, description: 'asdasdasd' });

/**
 * This function delete the product
 * @async
 * @function deleteProduct
 * @param {String} product
 * @returns Promise
 * @example
 *  deleteProduct('7 No')
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export const deleteProduct = async (name) => {
  try {
    const _product = await Products.findOneAndRemove(name);
    if (!_product) {
      throw new Error('Product does not exist');
    }
    return 'deleted';
  } catch (error) {
    throw error;
  }
};

// deleteProduct('7 No')
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
