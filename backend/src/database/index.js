/* eslint linebreak-style: ["error", "windows"] */

import mongoose from 'mongoose';

import { DB_URL } from '../config/index';

mongoose.Promise = global.Promise;

mongoose.set('debug', true);

try {
  mongoose.connect(DB_URL, {
    useNewUrlParser: true,
  });
} catch (error) {
  mongoose.connect(DB_URL, {
    useNewUrlParser: true,
  });
}

mongoose.connection
  .once('open', () => console.log('MongoDB is running'))
  .on('error', (e) => { throw e; });
