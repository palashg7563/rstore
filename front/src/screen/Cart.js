/* eslint linebreak-style: ["error", "windows"] */

import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { Box, Text } from 'react-native-design-utility';
import { inject } from 'mobx-react/native';


@inject('Cart')
class Cart extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Cart',
  })

  state={

  };

  render() {
    const { Cart } = this.props;
    const { products } = Cart;

    renderList = () => {
      if (products.length === 0) {
        return (
          <Box><Text>Cart Emtry</Text></Box>
        );
      }
      return (
        products.map((product) => {
          <Box key={product.id}>
            <Text>
              {product.name}
            </Text>
            <Text>
              Qunatity {product.cartQty}
            </Text>
          </Box>;
        })

      );
    };
    return (
      <Box f={1} center>
        <StatusBar barStyle="light-content" />
        <Text>
          Cart Screen
        </Text>
      </Box>
    );
  }
}


export default Cart;
