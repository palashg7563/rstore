/* eslint linebreak-style: ["error", "windows"] */
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { Alert } from 'react-native';
import { NavigatorServices } from '../api/NavigationServices';
import { theme } from '../constant/theme';
import TabBar from '../components/TabBar';

// different screen are imported
const LoginScreen = require('../screen/Login').default;
const HomeScreen = require('../screen/Home').default;
const CategoryScreen = require('../screen/Category').default;
const ListScreen = require('../screen/ListScreen').default;
const StoreScreen = require('../screen/Store').default;
const OrderScreen = require('../screen/Order').default;
const SplashScreen = require('../screen/SplashScreen').default;
const CartScreen = require('../screen/Cart').default;

const AuthNavigator = createStackNavigator({
  Login: {
    getScreen: () => LoginScreen,
  },
}, {
  navigationOptions: {
    header: null,
  },
});


const HomeStack = createStackNavigator({
  Home: {
    getScreen: () => HomeScreen,
  },
  Category: {
    getScreen: () => CategoryScreen,
  },
  ShoopingCart: {
    getScreen: () => CartScreen,
  },
}, {
  navigationOptions: {
    headerTintColor: theme.color.blue,
    headerBackTitleStyle: {
      fontWeight: '300',
    },
    headerStyle: {
      backgroundColor: theme.color.white,
      elevation: 1,
      shadowColor: theme.color.greyLight,
      shadowOpacity: 0.3,
    },

  },
});


const TabNavigator = createBottomTabNavigator(
  {
    Home: HomeStack,
    List: {
      getScreen: () => ListScreen,
    },
    Store: {
      getScreen: () => StoreScreen,
    },
    Order: {
      getScreen: () => OrderScreen,
    },
  }, {
    tabBarComponent: (props) => <TabBar {...props} />,
  },
);

const MainNavigator = createStackNavigator({
  Tab: TabNavigator,

}, {
  navigationOptions: {
    header: null,
  },
});

const AppNavigator = createSwitchNavigator(
  {
    Splash: {
      getScreen: () => SplashScreen,
    },
    Auth: AuthNavigator,
    Main: MainNavigator,
  }, {
    initialRouteName: 'Splash',
  },
);


class Navigation extends Component {
  render() {
    return (
      <AppNavigator
        ref={(r) => NavigatorServices.setTopLevelNavigation(r)}
      />
    );
  }
}

export default Navigation;
