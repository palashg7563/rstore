/* eslint linebreak-style: ["error", "windows"] */

import React, { Component } from 'react';
import { StatusBar, FlatList } from 'react-native';
import { Box } from 'react-native-design-utility';
import ProductCard from '../components/ProductCard';
import { theme } from '../constant/theme';

const NUMCOLUMNS = 1;

class Category extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('name'),
  });

  render() {
    const { navigation } = this.props;
    const products = navigation.getParam('products');

    return (
      <Box f={1}>
        <StatusBar
          networkActivityIndicatorVisible={false}
          translucent
          showHideTransition="slide"
          animated
          backgroundColor={theme.color.white}
          barStyle="light-content"
        />
        <Box f={1} bg="white">
          <FlatList
            data={products}
            renderItem={({ item, index }) => {
              const style = {};
              if (index % NUMCOLUMNS < NUMCOLUMNS) {
                style.borderBottomWidth = 2;
                style.borderBottomColor = theme.color.greyLightest;
              }
              if (index % NUMCOLUMNS != 0) {
                style.borderLeftWidth = 2;
                style.borderLeftColor = theme.color.greyLightest;
              }

              return (
                <Box w={1 / NUMCOLUMNS} bg="white" h={120} style={style}>
                  <ProductCard {...item} />
                </Box>
              );
            }}
            keyExtractor={(item) => String(item.id)}
            numColumns={NUMCOLUMNS}
          />
        </Box>
      </Box>
    );
  }
}

export default Category;
