import React, { Component } from 'react';
import { Box } from 'react-native-design-utility';

import { inject } from 'mobx-react/native';
import OnBoarding from '../components/onBoarding';

@inject('authstore')
class SplashScreen extends Component {
  componentDidMount() {
    this.checkAuth();
  }

  checkAuth=async () => {
    setTimeout(async () => {
      await this.props.authstore.setAuth();
    }, 1000);
  }

  render() {
    return (
      <Box f={1} center>
        <OnBoarding />
      </Box>
    );
  }
}

export default SplashScreen;
