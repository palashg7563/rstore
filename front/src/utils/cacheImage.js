/* eslint linebreak-style: ["error", "windows"] */

import { Image } from 'react-native';
import { Asset } from 'expo';

export function cacheImages(images) {
  return images.map((image) => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    }
    return Asset.fromModule(image).downloadAsync();
  });
}

export function caching(params) {
  console.log(params);
}
