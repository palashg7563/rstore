/* eslint linebreak-style: ["error", "windows"] */

export const images = {
  logo: require('../../assets/icon/brandLogo.png'),
  googleLogoIcon: require('../../assets/icon/googleIcon.png'),
};


export const tabBarIcons = {
  active:{
    Home:require('../../assets/icon/HomeIconActive.png'),
    List:require('../../assets/icon/ListIconActive.png'),
    Store:require('../../assets/icon/StoresIconActive.png'),
    Order:require('../../assets/icon/OrderIconActive.png'),
  },
  inactive:{
    Home:require('../../assets/icon/HomeIconInactive.png'),
    List:require('../../assets/icon/ListIconInactive.png'),
    Store:require('../../assets/icon/StoresIconInactive.png'),
    Order:require('../../assets/icon/OrderIconInactive.png'),
  }
};
