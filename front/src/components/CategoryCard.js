/* eslint linebreak-style: ["error", "windows"] */

import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Box, Text } from 'react-native-design-utility';
import { theme } from '../constant/theme';
import { NavigatorServices } from '../api/NavigationServices';

const style = StyleSheet.create({
  button: {
    flex: 1,
  },
});

const Grocery = [
  {
    id: 1,
    name: 'Allu',
    image: require('../../assets/icon/OrderIconActive.png'),
    description: 'satsa sundar tikau',
    price: 10,
  },
  {
    id: 2,
    name: 'pyaz',
    image: require('../../assets/icon/OrderIconActive.png'),
    description: 'rulane wala pyaz',
    price: 20,
  },
];

const Electronics = [
  {
    id: 1,
    name: 'Computer',
    description: 'dell ka nya wala computer',
    image: require('../../assets/icon/OrderIconActive.png'),
    price: 20000,
  },
  {
    id: 2,
    name: 'mouse',
    image: require('../../assets/icon/OrderIconActive.png'),
    description: 'tez wala mouse',
    price: 200,
  },
];

const products = (title) => {
  switch (title) {
    case 'Grocery': {
      return Grocery;
    }
    case 'Electronics': {
      return Electronics;
    }
    default:
      return {};
  }
};

class CategoryCard extends Component {
  state={

  };

  componentDidMount() {
    // console.log(`hi`);
  }

  handlePress = () => {
    const { title } = this.props;
    NavigatorServices.navigate('Category',
      {
        name: title,
        products: products(title),
      });
  };

  render() {
    const { image, title } = this.props;
    return (
      <TouchableOpacity onPress={this.handlePress} style={style.button}>
        <Box f={1} center>
          <Box mb="sn" center>
            <Image source={image} />
          </Box>
          <Box>
            <Text
              size="sm"
              color={theme.color.greyDarker}
              capitalizeEach
              center
            >
              {title}
            </Text>
          </Box>
        </Box>
      </TouchableOpacity>
    );
  }
}

export default CategoryCard;
