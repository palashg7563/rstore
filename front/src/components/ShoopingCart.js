/* eslint linebreak-style: ["error", "windows"] */

import React, { Component } from 'react';
import { Box, Text } from 'react-native-design-utility';
import { FontAwesome } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native';
import { NavigatorServices } from '../api/NavigationServices';
import { theme } from '../constant/theme';

class ShoopingCart extends Component {
  state={}

  render() {
    return (
      <TouchableOpacity
        onPress={() => NavigatorServices.navigate('ShoopingCart')}
      >
        <Box f={1}>
          <Box f={1} mr={10} center>
            <Text>
              <FontAwesome
                name="shopping-cart"
                size={20}
                // style={{ paddingRight: 10 }}
                color={theme.color.blue}
              />
            </Text>
          </Box>
        </Box>
      </TouchableOpacity>
    );
  }
}

export default ShoopingCart;
