/* eslint linebreak-style: ["error", "windows"] */
/* eslint import/prefer-default-export:0 */

import { AsyncStorage } from 'react-native';
import { types, flow } from 'mobx-state-tree';
import { NavigatorServices } from '../api/NavigationServices';

import { customerApi } from '../api/customer';
import { UserInfo } from '../models/CurrentUser';

const TOKEN_KEY = '@rstore/key';

export const AuthStore = types.model('AuthStore', {
  authToken: types.maybe(types.string),
  info: types.maybe(UserInfo),
}).actions((self) => ({
  setAuth: flow(function* setAuth() {
    yield self.getAuthToken();
    yield self.getUserInfo();
  }),
  saveToken: flow(function* saveToken(token) {
    try {
      yield AsyncStorage.setItem(TOKEN_KEY, token);
    } catch (error) {
      console.error(error);
    }
  }),
  getAuthToken: flow(function* getAuthToken() {
    try {
      const token = yield AsyncStorage.getItem(TOKEN_KEY);

      if (token) {
        self.authToken = token;
      } else {
        NavigatorServices.navigate('Auth');
      }
    } catch (error) {
      console.error(error);
    }
  }),
  getUserInfo: flow(function* getUserInfo() {
    try {
      if (self.authToken) {
        const res = yield customerApi.url('/me')
          .headers({ Authorization: `Bearer ${self.authToken}` })
          .get()
          .json();

        self.info = res;

        NavigatorServices.navigate('Main');
      }
    } catch (error) {
      console.error(error);
    }
  }),
  login: flow(function* login(providerToken, provider) {
    try {
      const res = yield customerApi
        .post({
          token: providerToken,
          provider,
        })
        .json();

      if (res.token) {
        self.authToken = res.token;
        yield self.saveToken(res.token);
        yield self.getUserInfo();
      }
    } catch (error) {
      console.log('error', error);
    }
  }),
}));
