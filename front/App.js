import React, { Component } from 'react';
import { UtilityThemeProvider, Box } from 'react-native-design-utility';
import { ActivityIndicator } from 'react-native';
import { Provider } from 'mobx-react/native';

// Screen
import Navigation from './src/screen/index';
import { theme } from './src/constant/theme';
import { images, tabBarIcons } from './src/constant/image';

// utils functions
import { cacheImages } from './src/utils/cacheImage';

// mobx store
import { store } from './src/store/index';

export default class App extends Component {
  state = {
    ready: false,
  };

  componentDidMount() {
    this.cacheAssest();
  }

  cacheAssest = async () => {
    // Takes the array of the images in require format and return the promise
    const cacheAssetimages = cacheImages([
      ...Object.values(images),
      ...Object.values(tabBarIcons.inactive),
      ...Object.values(tabBarIcons.active),
    ]);

    await Promise.all([...cacheAssetimages]);

    this.setState({
      ready: true,
    });
  }

  render() {
    const { ready } = this.state;
    if (!ready) {
      return (
        <Box f={1} center bg="white">
          <ActivityIndicator size="large" />
        </Box>
      );
    }

    return (
      <Provider {...store}>
        <UtilityThemeProvider theme={theme}>
          <Navigation />
        </UtilityThemeProvider>
      </Provider>
    );
  }
}
